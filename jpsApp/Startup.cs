﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(jpsApp.Startup))]
namespace jpsApp
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
