﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="Register3.aspx.cs" Inherits="jpsApp.Account.Register3" %>



<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SB Admin 2 - Register</title>

  <!-- Custom fonts for this template-->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link href="../Content/theme/css/sb-admin-2.css" rel="stylesheet" />
  <!-- Custom styles for this template-->
  <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Register Premise</h1>
              </div>
              <form class="user" runat="server">
                <div class="form-group row">
                    <p class="text-danger">
                        <asp:Literal runat="server" ID="ErrorMessage" />
                    </p>
                  <div class="col-sm-6 mb-3 mb-sm-0">
                     <asp:TextBox runat="server" ID="FirstName" CssClass="form-control form-control-user" placeholder="First Name"></asp:TextBox>
                      <asp:RequiredFieldValidator runat="server" ControlToValidate="FirstName"
                                                  CssClass="text-danger" ErrorMessage="First name is required." />
                  
                      </div>
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <asp:TextBox runat="server" ID="LastName" CssClass="form-control form-control-user" placeholder="First Name"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="LastName"
                                                    CssClass="text-danger" ErrorMessage="last name is required." />
                  
                    </div>
                </div>
                <div class="form-group">
                    <asp:TextBox runat="server" ID="Email" placeholder="Email Address" CssClass="form-control form-control-user"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                                                CssClass="text-danger" ErrorMessage="The email field is required." />
                </div>
                  
                  <div class="form-group">
                      <asp:TextBox runat="server"  placeholder="Premise Number" ID="PremiseNumber" CssClass="form-control form-control-user"></asp:TextBox>
                      <div class="col-sm-6 mb-3 mb-sm-0">
                          <asp:RequiredFieldValidator runat="server" ControlToValidate="PremiseNumber"
                                                      CssClass="text-danger" ErrorMessage="Premise number is required." />
                  
                      </div>
                      <br />
                      <asp:TextBox runat="server" placeholder="Premise's Address" CssClass="form-control form-control-user" ID="Address" ></asp:TextBox>
                      <asp:RequiredFieldValidator runat="server" ControlToValidate="Address"
                                                  CssClass="text-danger" ErrorMessage="Address is required." />
                     
                  </div>
                  

                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                      <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control form-control form-control-user" placeholder="Password"/>
                      <asp:RequiredFieldValidator runat="server" ControlToValidate="Password"  CssClass="text-danger"  ErrorMessage="The password field is required." />                  

                  </div>

                  <div class="col-sm-6">
                      
                      <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control form-control-user" placeholder="Confirm Password"/>
                      <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword"
                                                  CssClass="text-danger" Display="Dynamic" ErrorMessage="The confirm password field is required." />
                      <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                                            CssClass="text-danger" Display="Dynamic" ErrorMessage="The password and confirmation password do not match." />
                  </div>
                </div>
               
                <hr>
                  <div class="text-center">
                      <asp:Button runat="server"  Text="Register" CssClass="btn btn-success" OnClick="CreateUser_Click"/>
                  </div>
           
              </form>
              <hr>
              <div class="text-center">
                <a class="small" href="Login.aspx">Login Instead</a>
              </div>
            
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

</body>

</html>

