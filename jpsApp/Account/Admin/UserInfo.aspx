﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UserInfo.aspx.cs" Inherits="jpsApp.Account.Admin.UserInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <asp:Label runat="server" hidden="hidden" ID="userId"></asp:Label>
    <div class="card col-sm-8 offset-sm-2" style="margin-top: 20px;">
        <div class="card-header">
            User Information
        </div>

        <div class="card-body .form-horizontal">
            <div class="form-group">
                <label class="">Name:</label>
                <asp:Label runat="server" CssClass="form-control" ID="FullName"></asp:Label>
            </div>
            <div class="form-group">
                <label>Account Number:</label>
                <asp:Label runat="server" ID="AccountNumber" CssClass="form-control" />

            </div>
            <div class="form-group">
                <label class=" ">Email:</label>
                <asp:Label runat="server" CssClass="form-control" ID="Email" />


            </div>
            <div class="form-group">
                <br />
                <label>Balance:</label>

                <asp:Label runat="server" CssClass="form-control" ID="Balance"> </asp:Label><br />
                <asp:Label runat="server" hidden="true" ID="PremiseId"></asp:Label>
            </div>




        
            <div class="form-group">
                <asp:Button Text="Generate Bill" runat="server" CssClass="btn btn-success" ID="generateBill" OnClick="generateCustomerBill" />
                <asp:Button Text="Payment History" runat="server" CssClass="btn btn-warning" ID="paymentHistory" />
                <asp:Button Text="View Outstanding bill(s)" runat="server" CssClass="btn btn-danger" ID="outstandingBills" OnClick="getOustandingBills" />

            </div>
        </div>
    </div>
    <br />

    '<div class="row" style="margin-top: 5px;">
        <div class="card col-8 offset-2 mb-4" hidden="hidden" runat="server" id="successCard">
            <div class="card-header">
                <h1>Bill Generated Sucessfuly</h1>
                <small>Details Below</small>
            </div>
            <div class="card-body">
                <asp:Label runat="server" ID="BillId"> </asp:Label><br />
                <asp:Label runat="server" ID="BillCustomerID"> </asp:Label><br />
                <asp:Label runat="server" ID="BillPremiseId"> </asp:Label><br />
                <asp:Label runat="server" ID="BillAmount"> </asp:Label><br />
                <asp:Label runat="server" ID="TotalAmountDue"> </asp:Label><br />
                <asp:Label runat="server" ID="DateIssued"> </asp:Label><br />
                <asp:Label runat="server" ID="DateDue"> </asp:Label><br />
            </div>
        </div>
        
        
        <div class="card col-12 offset-0 mb-4"  runat="server" id="oustanddingBalanceCard" hidden="hidden">
            <div class="card-header">
                <div class="alert alert-primary" role="alert">
                    <h1><asp:Label runat="server" ID="outstandingBillsHeader"></asp:Label></h1>
                    <br />
                    
                    <small>Details Below</small>
                </div>
            </div>
            <div class="card-body">
                <asp:GridView runat="server" OnRowEditing="gridViewOutstandingBalance_OnRowEditing" OnRowUpdated="gridViewOutstandingBalance_OnRowUpdated" ID="gridViewOutstandingBalance" AutoGenerateColumns="False" DataSourceID="oustStandingBillsSource" AllowPaging="True" PageSize="5" CssClass="table table-responsive table-striped table-hover" AllowSorting="True" OnRowDeleting="gridViewOutstandingBalance_RowDeleting" OnPageIndexChanged="gridViewOutstandingBalance_PageIndexChanged">
                    <Columns>
                        <asp:CommandField ShowDeleteButton="True" ShowEditButton="True"  />
                        <asp:BoundField DataField="Account Number" HeaderText="Account Number" ReadOnly="True"/>
                        <asp:BoundField DataField="Bill Id" HeaderText="Bill #" ReadOnly="True" />
                        <asp:BoundField DataField="Premise Id" HeaderText="Premise ID" ReadOnly="True"/>
                        <asp:BoundField DataField="Amount" HeaderText="Amount" />
                        <asp:BoundField DataField="Date Issued" HeaderText="Issued On" ReadOnly="True"/>
                        <asp:BoundField DataField="Due Date" HeaderText="Due Date"/>
                        <asp:BoundField DataField="Paid" HeaderText="Paid For" ReadOnly="True"/>
                    </Columns>
                    
                </asp:GridView>
                
                
               <asp:SqlDataSource ID="oustStandingBillsSource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"  SelectCommand="execute getOustandingBills @AccountNumber" DeleteCommand="delete from Bills where BillId =@deleteParameter">
                   <SelectParameters>
                       <asp:QueryStringParameter Name="AccountNumber" />

                   </SelectParameters>
                   
                  
               </asp:SqlDataSource>
                

            </div>
        </div>
    </div>

 



    <asp:Label runat="server" ID="errorMessage" hidden="hidden"></asp:Label>


 
</asp:Content>
