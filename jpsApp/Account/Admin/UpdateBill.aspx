﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UpdateBill.aspx.cs" Inherits="jpsApp.Account.Admin.UpdateBill" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="card mt-5 mb-2">
        <div class="card-header">
            <h1 class="card-title" runat="server" id="billHeader">Update Bill</h1>
            

        </div>

        <div class="card-body col col-sm-10 offset-sm-1" id="cardBody" runat="server">
            <div class="form-group-sm row">
                <asp:Label Text="Customer Name" runat="server" />&nbsp;
                <asp:Label  runat="server" ID="lblCustomerName" CssClass="form-control"/>&nbsp;
            </div>
            <div class="form-group-sm row">
                <asp:Label Text="Premise " runat="server" />&nbsp;
                <asp:Label  runat="server" ID="lblPremise" CssClass="form-control"/>&nbsp;
            </div>

            <div class="form-group-sm row">
                <asp:Label Text="Bill Number" runat="server" />&nbsp;
                <asp:Label  runat="server" ID="lblBillId" CssClass="form-control"/>&nbsp;
            </div>
            
            <div class="form-group-sm row">
                <asp:Label Text="Issued On" runat="server" />&nbsp;
                <asp:Label  runat="server" ID="lblDateIssued" CssClass="form-control"/>&nbsp;
            </div>
            
            <div class="form-group-sm row">
                <asp:Label Text="Amount" runat="server" />&nbsp;
                <asp:TextBox  runat="server" ID="txtAmount" CssClass="form-control"/>&nbsp;
            </div>
            
            <div class="form-group-sm row">
                <asp:Label Text="Due Date" runat="server" />&nbsp;
                <asp:TextBox  runat="server" ID="txtDueDate" CssClass="form-control" TextMode="Date"/>&nbsp;
                <%--<input type="date" name="name" value="" id="dDAte" runat="server"/>--%>
            </div>
            
            <div class="form-group-sm row">
                <asp:Label Text="Status" runat="server" />&nbsp;
                <asp:Label Text="Not Paid"  runat="server" ID="lblStatus" CssClass="form-control"/>&nbsp;
            </div>
            
            <hr />
            <div class="form-group">
                <asp:Button Text="Update" runat="server"  CssClass="btn btn-warning" ID="btnUpdate" OnClick="btnUpdate_OnClick"/>
            </div>
            
            
        </div>
    </div>
</asp:Content>
