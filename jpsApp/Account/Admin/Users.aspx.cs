﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace jpsApp.Account.Admin
{
    public partial class Users : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.IsInRole("Admin"))
            {
                Response.Redirect("~/Account/Login.aspx");
            }

            //usersGridView.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow selectedRow = usersGridView.SelectedRow;

            String[] userInfo= new String[]
            {
                selectedRow.Cells[1].Text, //email
                selectedRow.Cells[2].Text,//account number
                selectedRow.Cells[3].Text, //first name
                selectedRow.Cells[4].Text, //last name
                selectedRow.Cells[5].Text, // balance
                selectedRow.Cells[6].Text //premise number

            };

            Session["user"] = userInfo;

            Response.Redirect("UserInfo");

        }
    }
}