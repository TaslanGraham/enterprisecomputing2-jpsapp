﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="jpsApp.Account.Admin.Users" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    
    <div class="row">
    <div class="card col" style="margin-top: 80px;">
        <div class="card-header">
            <div class="alert alert-primary">
                <h2>Select a user to view their bills information</h2>
            </div>
        </div>
        <div class="card-body col-sm-10 offset-sm-1">
               <asp:GridView ID="usersGridView" runat="server" CssClass="table table-responsive table-striped table-hover" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="UsersDatasource" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
          
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />
                <asp:BoundField DataField="Account Number" HeaderText="Account Number" InsertVisible="False" ReadOnly="True" SortExpression="Account Number" />
                <asp:BoundField DataField="First Name" HeaderText="First Name" SortExpression="First Name" />
                <asp:BoundField DataField="Last Name" HeaderText="Last Name" SortExpression="Last Name" />
                <asp:BoundField DataField="Balance" HeaderText="Balance" SortExpression="Balance" />
                <asp:BoundField DataField="Premise Number" HeaderText="Premise Number" SortExpression="Balance" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="UsersDatasource" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="Select Email, AccountNumber as [Account Number],  FirstName as [First Name], LastName as [Last Name], OutstandingBalance as [Balance], PremiseId as [Premise Number] from CustomerAccounts inner join AspNetUsers on CustomerAccounts.CustomerId = AspNetUsers.Id inner join Premises on CustomerAccounts.CustomerId = Premises.CustomerId inner join AspNetUserRoles on AspNetUserRoles.UserId = CustomerAccounts.CustomerId where AspNetUserRoles.RoleId!=1;"></asp:SqlDataSource>

        </div>
    </div>

    </div>
   
    
  <script src="Scripts/bootstrap.min.js"></script>

    <script src="Scripts/bootstrap.js"></script>
    </asp:Content>

