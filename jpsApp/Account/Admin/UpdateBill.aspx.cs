﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using jpsApp.Models;

namespace jpsApp.Account.Admin
{
    public partial class UpdateBill : System.Web.UI.Page
    {
        Models.UtilityClass utilityMethods = new UtilityClass();
        private long billId = 0;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Session["bill"] == null)
                {
                    Response.Redirect("Users");
                }
                else
                {
                    billId = Convert.ToInt64(Session["bill"].ToString());
                }

                using (SqlConnection connection = new SqlConnection(utilityMethods.getConnectionString()))
                {
                    SqlCommand query = new SqlCommand();
                    query.Connection = connection;

                    query.CommandText =
                        "Select FirstName,LastName,BillId, PremiseId,DateIssued,DueDate,Amount from Bills Inner join CustomerAccounts on Bills.CustomerId = CustomerAccounts.CustomerId WHERE BillId = @billId;";
                    query.Parameters.AddWithValue("@billId", Session["bill"].ToString());

                    connection.Open();
                    SqlDataReader rdr = query.ExecuteReader();

                    if (!rdr.HasRows)
                    {
                        billHeader.InnerText = "None of our records matched the selected bill";
                        billHeader.Attributes.Add("color", "red");
                        cardBody.Attributes.Add("hidden", "hidden");
                    }
                    else
                    {

                        while (rdr.Read())
                        {
                            txtAmount.Text = rdr["Amount"].ToString();
                            lblBillId.Text = rdr["BillId"].ToString();
                            lblCustomerName.Text = (rdr["FirstName"] + " " + rdr["LastName"]).ToString();
                            lblDateIssued.Text = Convert.ToDateTime(rdr["DateIssued"]).ToLongDateString();

                            DateTime dueDate = new DateTime();
                            dueDate = Convert.ToDateTime(rdr["DueDate"].ToString());
                            txtDueDate.Text = dueDate.ToString("yyyy-MM-dd");
                            lblPremise.Text = rdr["PremiseId"].ToString();
                            lblStatus.Text = "Not Paid";

                        }

                    }

                }

            }

        }



        protected void btnUpdate_OnClick(object sender, EventArgs e)
        {
            DateTime dueDate = new DateTime();
            dueDate = Convert.ToDateTime(txtDueDate.Text);
            Decimal amount = Convert.ToDecimal(txtAmount.Text);

            using (SqlConnection connection = new SqlConnection(utilityMethods.getConnectionString()))
            {

                SqlCommand query = new SqlCommand();
                query.Connection = connection;
                query.CommandText = "UPDATE Bills SET Amount =@amount, DueDate=@dueDate, UpdatedAt=GETDATE() WHERE BillId=@id";
                query.Parameters.AddWithValue("@amount", amount.ToString("####"));
                query.Parameters.AddWithValue("@dueDate", dueDate.ToLongTimeString());
                query.Parameters.AddWithValue("@id", lblBillId.Text);

                connection.Open();

                int count = query.ExecuteNonQuery();

                if (count == 1)
                {
                    billHeader.InnerText = "Bill Update Successfully!";
                    billHeader.Attributes.Add("color", "red");
                }
                else
                {

                    billHeader.InnerText = "Ooops! Something went wrong. Please Try again";
                    billHeader.Attributes.Add("color", "red");
                }

            }
        }
    }
}