﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using jpsApp.Models;

namespace jpsApp.Account.Admin
{
    public partial class UserInfo : System.Web.UI.Page
    {
        Models.UtilityClass utilityMethods = new UtilityClass();
        SqlCommand query = new SqlCommand();
       

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!User.IsInRole("Admin") || Session["user"] == null)
            {
                Response.Redirect("~/Account/Login");
            }

            if (Session["user"] != null)
            {

                String[] user = (String[])Session["user"];
                Email.Text = user[0];
                AccountNumber.Text = user[1];
                FullName.Text = user[2] + " " + user[3];
                Balance.Text = "Outstanding Balance: " + Convert.ToDecimal(user[4]).ToString("C");
                PremiseId.Text = user[5];
                gridViewOutstandingBalance.DataBind();

               
         
            }

        }


        protected void generateCustomerBill(object sender, EventArgs e)
        {



            using (SqlConnection connection = new SqlConnection(utilityMethods.getConnectionString()))
            {
                DateTime date = new DateTime();

                query.Connection = connection;
                query.CommandText = "Select CustomerAccounts.CustomerId, Email, AccountNumber as [Account Number],  FirstName as [First Name], LastName as [Last Name], OutstandingBalance as [Balance], PremiseId as [Presmise Number] from CustomerAccounts inner join AspNetUsers on CustomerAccounts.CustomerId = AspNetUsers.Id inner join Premises on CustomerAccounts.CustomerId = Premises.CustomerId where CustomerAccounts.AccountNumber = @AccountNumber";
                query.Parameters.AddWithValue("@AccountNumber", AccountNumber.Text);
                connection.Open();
                SqlDataReader dataRdr = query.ExecuteReader();

                String premiseId = PremiseId.Text;
                String Email;
                String accountNumber = AccountNumber.Text;
                String fullName;
                DateTime dueDate = new DateTime();
                dueDate = DateTime.Today.AddDays(3).AddMonths(1);
                String CustomerId = "";
                Random rnd = new Random();
                Decimal amount = rnd.Next(2000, 35000);
                Decimal balance = 0;

                if (dataRdr.HasRows)
                {
                    while (dataRdr.Read())
                    {
                        CustomerId = dataRdr["CustomerId"].ToString();
                        balance = Convert.ToDecimal(dataRdr["Balance"].ToString());
                        Email = dataRdr["Email"].ToString();
                        //accountNumber = dataRdr["Account Number"].ToString();
                        fullName = dataRdr["First Name"].ToString() + " " + dataRdr["last Name"].ToString();

                    }

                    dataRdr.Close();


                    SqlCommand billQuery = new SqlCommand();
                    billQuery.Connection = connection;

                    billQuery.CommandText = "INSERT INTO Bills(CustomerId,PremiseId,Amount,DateIssued,DueDate,IsPaid) VALUES(@CustomerId, @PremiseId, @Amount, @dateIssued, @DueDate, 0) ";


                    billQuery.Parameters.AddWithValue("@CustomerId", CustomerId.ToString());
                    billQuery.Parameters.AddWithValue("@PremiseId", premiseId);
                    billQuery.Parameters.AddWithValue("@Amount", amount);
                    billQuery.Parameters.AddWithValue("@dateIssued", date.ToLongTimeString());
                    billQuery.Parameters.AddWithValue("@DueDate", dueDate);


                    int count = Convert.ToInt32(billQuery.ExecuteNonQuery());

                    if (count == 1)
                    {
                        SqlCommand updateBalance = new SqlCommand();
                        updateBalance.Connection = connection;
                        updateBalance.CommandText = "UPDATE CustomerAccounts SET OutstandingBalance = [OutstandingBalance] + @Amount where CustomerAccounts.CustomerId = @id";
                        updateBalance.Parameters.AddWithValue("@Amount", amount);
                        updateBalance.Parameters.AddWithValue("@Id", CustomerId);

                        int resultCount = Convert.ToInt32(updateBalance.ExecuteNonQuery());

                        if (resultCount == 1)
                        {

                            successCard.Attributes.Remove("hidden");
                            BillId.Text = "Bill ID: 788";
                            BillCustomerID.Text = "Customer Account: " + AccountNumber.Text;
                            BillAmount.Text = "Bill Amount: " + amount.ToString();
                            TotalAmountDue.Text = "Total Due: " + (balance + amount).ToString("C");
                            DateDue.Text = "Due Date: " + dueDate.ToLongDateString();
                            Balance.Text = (balance + amount).ToString("C");
                        }
                    }
                }
                else
                {
                    errorMessage.Attributes.Remove("Hidden");
                    errorMessage.Text = "Oops! Something Went Wrong. Please Try Again!";
                }

            }
        }

        protected void getOustandingBills(object sender, EventArgs e)
        {
         
            oustStandingBillsSource.SelectParameters[0].DefaultValue = AccountNumber.Text;
            oustanddingBalanceCard.Attributes.Remove("hidden");

            if (gridViewOutstandingBalance == null)
            {
                outstandingBillsHeader.Text = "Ths Customer Has no outstanding bill(s).";
                outstandingBillsHeader.Attributes.Add("color", "red");
            }
            else
            {
                outstandingBillsHeader.Text = "Oustanding Bills for " + FullName.Text;
               // errorMessage.Attributes.Remove("hidden");
            }


        }

        protected void gridViewOutstandingBalance_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(utilityMethods.getConnectionString()))
            {
                TableCell cell = gridViewOutstandingBalance.Rows[e.RowIndex].Cells[2];
                TableCell DeletedAmount = gridViewOutstandingBalance.Rows[e.RowIndex].Cells[4];
                query.Connection = connection;

                connection.Open();

                oustStandingBillsSource.DeleteCommandType = SqlDataSourceCommandType.Text;
                oustStandingBillsSource.DeleteCommand = "delete from Bills where BillId = " + cell.Text;

                if (Convert.ToInt32(oustStandingBillsSource.Delete()) == 1)
                {
                    query.CommandText = "Update CustomerAccounts SET OutstandingBalance = [OutstandingBalance] -" + DeletedAmount.Text + "where AccountNumber = " + AccountNumber.Text;
                    query.ExecuteNonQuery();

                }
                //query.Parameters.AddWithValue("@id", cell.Text);
                //int count = 
                //if (count == 1)
                //{
                //    errorMessage.Attributes.CssStyle.Add("color", "red");
                //    errorMessage.Text = "Succesfuly Deleted Bill " + cell.Text;
                //}
            }
        }

        protected void gridViewOutstandingBalance_PageIndexChanged(object sender, EventArgs e)
        {
            oustanddingBalanceCard.Attributes.Remove("hidden");
        }

      



        //protected void updateUserFormValues(String ac)
        //{
        //    using (SqlConnection connection = new SqlConnection(utilityMethods.getConnectionString()))
        //    {
        //        query.Connection = connection;
        //        query.CommandText ="Select CustomerAccounts.CustomerId, Email, AccountNumber as [Account Number],  FirstName as [First Name], LastName as [Last Name], OutstandingBalance as [Balance], PremiseId as [Presmise Number] from CustomerAccounts inner join AspNetUsers on CustomerAccounts.CustomerId = AspNetUsers.Id inner join Premises on CustomerAccounts.CustomerId = Premises.CustomerId where CustomerAccounts.AccountNumber = @AccountNumber";
        //        query.Parameters.AddWithValue("@AccountNumber", ac);
        //        connection.Open();
        //        SqlDataReader dataRdr = query.ExecuteReader();



        //        if (dataRdr.HasRows)
        //        {
        //            while (dataRdr.Read())
        //            {
        //                customerId = dataRdr["CustomerId"].ToString();
        //                premiseId = dataRdr["Presmise Number"].ToString();
        //                balance = Convert.ToDecimal(dataRdr["Balance"].ToString());
        //                email = dataRdr["Email"].ToString();
        //                //accountNumber = dataRdr["Account Number"].ToString();
        //                fullName = dataRdr["First Name"].ToString() + " " + dataRdr["last Name"].ToString();

        //            }
        //        }
        //    }
        //}


        protected void gridViewOutstandingBalance_OnRowUpdated(object sender, GridViewUpdatedEventArgs e)
        {
            if (e.AffectedRows > 0)
            {
                Decimal amount = Convert.ToDecimal(e.Keys["Amount"].ToString());
                DateTime dueDate = new DateTime();
                dueDate = Convert.ToDateTime(e.Keys["Due Date"]);
                oustStandingBillsSource.DeleteCommandType = SqlDataSourceCommandType.Text;
                oustStandingBillsSource.Update();

            }
        }

        protected void gridViewOutstandingBalance_OnRowEditing(object sender, GridViewEditEventArgs e)
        {
            Decimal billId = Convert.ToDecimal(gridViewOutstandingBalance.Rows[e.NewEditIndex].Cells[2].Text);
            Session["bill"] = null;
            Session["bill"] = billId;
            Response.Redirect("UpdateBill");
        }
    }
}