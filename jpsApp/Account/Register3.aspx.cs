﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using jpsApp.Models;

namespace jpsApp.Account
{
    public partial class Register3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }



        protected void CreateUser_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var signInManager = Context.GetOwinContext().Get<ApplicationSignInManager>();


            var user = new ApplicationUser() { UserName = Email.Text, Email = Email.Text };
            IdentityResult result = manager.Create(user, Password.Text);
            if (result.Succeeded)
            {

                Models.Customer customer = new Models.Customer();
                Models.UtilityClass utilityMethods = new Models.UtilityClass();
                customer.firstName = FirstName.Text;
                customer.lastName = LastName.Text;


                using (SqlConnection connection = new SqlConnection(utilityMethods.getConnectionString()))
                {
                    SqlCommand query = new SqlCommand();
                    query.Connection = connection;
                   

                    query.CommandText = "execute RegisterCustomerAndProperty @UserId, @FirstName,@LastName, @PremisesNumber,@PremisesAddress";
                  
                    query.Parameters.AddWithValue("@UserId", user.Id);
                    query.Parameters.AddWithValue("@FirstName", customer.firstName);
                    query.Parameters.AddWithValue("@LastName", customer.lastName);
                    query.Parameters.AddWithValue("@PremisesNumber", PremiseNumber.Text);
                    query.Parameters.AddWithValue("@PremisesAddress", Address.Text);
                    connection.Open();
                    if (query.ExecuteNonQuery() > 0)
                    {
                        if (!manager.IsInRole(user.Id, "Customer"))
                        {
                            manager.AddToRole(user.Id, "Customer");
                        }
                        signInManager.SignIn(user, isPersistent: false, rememberBrowser: false);
                        IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                    }


                }

           
            }
            else
            {
                ErrorMessage.Text = result.Errors.FirstOrDefault();
            }
        }
    }
}