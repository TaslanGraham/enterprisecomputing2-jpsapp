﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace jpsApp.Models
{
    public class Customer
    {
        public String customerId { set; get; }
        public int accountNumber {set; get;}
        public String firstName { set; get; }
        public String lastName { set; get; }
        public decimal outstandingBalance { set; get; }

    }
}