use jpsDatabase;



Create table CustomerAccounts(
	CustomerId NVARCHAR(128) not null primary key,
	AccountNumber int not null identity(1200064, 1),
	FirstName varchar(255) not null,
	LastName varchar(255) not null,
	OutstandingBalance money not null,

	constraint fk_key FOREIGN KEY (CustomerId) references AspNetUsers(Id) on delete cascade ,
);

create table Premises(
	PremiseId varchar(255) not null primary key,
	CustomerId NVARCHAR(128) not null,
	PremiseAddress varchar(255),
	DateAdded date,
	constraint p_fk FOREIGN KEY (CustomerId) references CustomerAccounts(CustomerId) on delete cascade
);

create table Bills(
	BillId int  not null identity(17000000, 1) primary key,
	CustomerId NVARCHAR(128) not null,
	PremiseId varchar(255) not null,
	Amount money not null,
	DateIssued date not null,
	DueDate date not null,
	IsPaid bit not null,
	UpdatedAt date

	constraint fk_premiseid FOREIGN KEY (PremiseId) references Premises(PremiseId) on update cascade,

	constraint fk_premiseid2 FOREIGN KEY (PremiseId) references Premises(PremiseId) on delete cascade,
	constraint fk_customerid FOREIGN KEY (CustomerId) references CustomerAccounts(CustomerId) on update cascade
);

create table BillPayments(
	BillId int not null,


);


--=================== STORED PROCEDURES/ TRANSACTIONS ======================
GO 
	CREATE PROCEDURE getOustandingBills
		@AccountNumber int

AS

BEGIN
	select AccountNumber as [Account Number],BillId as [Bill Id], PremiseId as [Premise Id], Amount,DateIssued as [Date Issued], DueDate as [Due Date] , Paid ='No' 
	from CustomerAccounts
	inner join
	Bills
	on Bills.CustomerId = CustomerAccounts.CustomerId 
	where IsPaid=0 and CustomerAccounts.AccountNumber = @AccountNumber;

END

GO
	CREATE PROCEDURE GenerateBill
		@CustomerId NVARCHAR(128),
		@PremiseId varchar(255),
		@Amount Money,
		@DueDate date

AS 
	BEGIN
		INSERT INTO Bills VALUES(@CustomerId, @PremiseId, @Amount, GETDATE(), @DueDate, 0);

	
	END




GO
	CREATE PROCEDURE RegisterCustomerAndProperty
		@UserId NVARCHAR(128),
		@FirstName varchar(255),
		@LastName varchar(255),
		@PremisesNumber varchar(255),
		@PremisesAddress varchar(255)

AS
BEGIN
	BEGIN TRANSACTION
		INSERT INTO CustomerAccounts(CustomerId,FirstName, LastName, OutstandingBalance) VALUES(@UserId, @FirstName, @LastName, 0)
			IF(@@ERROR <>0)
				BEGIN
					ROLLBACK
				END
			else IF (@@ERROR =0)

				INSERT INTO Premises VALUES (@PremisesNumber ,@UserId, @PremisesAddress, GETDATE())
					IF(@@ERROR = 0)
						BEGIN
							COMMIT TRANSACTION 
						END
					else rollbaCK
	
END

select * from CustomerAccounts
--execute RegisterCustomerAndProperty '3a48a3f9-2060-4799-9fa7-3463b4af73e5', 'Taslan','Graham', '5853e5456','Prospect';
--select * from AspNetUsers

--Select * from Premises
--Select * from AspNetRoles
--select * from AspNetUserRoles where UserId='69362d26-d2e5-4420-9111-98769b3e2b13'
--execute getOustandingBills  1200076;
--UPDATE Bills SET Amount =6, DueDate = '08-08-2000' WHERE BillId = 17000039
--select * from Bills where Amount = 6
--Select FirstName,LastName,BillId, PremiseId,DateIssued,DueDate,Amount from Bills Inner join CustomerAccounts on Bills.CustomerId = CustomerAccounts.CustomerId WHERE BillId=17000001;
--select * from Premises where CustomerId='3a48a3f9-2060-4799-9fa7-3463b4af73e5'
--select * from AspNetUserRoles
--UPDATE BILLS SET Amount=49, DateIssued='02/08/2019' where BillId=17000001;
--execute GenerateBill '3a48a3f9-2060-4799-9fa7-3463b4af73e5','5853e5456',45354,'01-01-2018';
--Select Email,CustomerAccounts.CustomerId, AccountNumber as [Account Number],  FirstName as [First Name], LastName as [Last Name], OutstandingBalance as [Balance], PremiseId as [Presmise Number] from CustomerAccounts inner join AspNetUsers on CustomerAccounts.CustomerId = AspNetUsers.Id inner join Premises on CustomerAccounts.CustomerId = Premises.CustomerId where CustomerAccounts.AccountNumber = 1200076;
--Select Email, AccountNumber as [Account Number],  FirstName as [First Name], LastName as [Last Name], OutstandingBalance as [Balance], PremiseId as [Presmise Number] from CustomerAccounts inner join AspNetUsers on CustomerAccounts.CustomerId = AspNetUsers.Id inner join Premises on CustomerAccounts.CustomerId = Premises.CustomerId;